describe("Login mdCSA", () => {
  it("Login", function() {
    let mdToken;
    cy.request({
      method: "POST",
      url: "https://mcs-develop.appspot.com/auth/v3/login",
      body: {
        email: "steven.gomez@lp360.com",
        password: "rwz2bac5uqc3FRZ!xwu",
      },
    }).then((response) => {
      expect(response.status).to.eq(200);
      expect(response.body).to.have.property("token");
      let mdToken = response.body.token
      cy.wrap(mdToken).as("mdToken");
    });
  });
});
