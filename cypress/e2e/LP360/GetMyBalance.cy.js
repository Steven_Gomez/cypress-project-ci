let token;

describe("Login PAM", () => {
  it("Login mdCSA for PAM", function () {
    cy.request({
      method: "POST",
      url: "https://mcs-develop.appspot.com/auth/v3/login",
      body: {
        email: "steven.gomez@lp360.com",
        password: "rwz2bac5uqc3FRZ!xwu",
      },
    }).then((response) => {
      expect(response.status).to.eq(200);
      expect(response.body).to.have.property("token");
      let bearerToken = response.body.token;
      cy.wrap(bearerToken).as("bearerToken");
    });
  });

  /*
  it("Get My Balance", function () {
    token = this.bearerToken;

    cy.request({
      method: "GET",
      url: "https://accounting-develop.online.lp360.com/accounting/v1/wallet/balance",
      headers: {
        "md-csa-application-id": "Token-Payment-Microservice",
        "md-csa-application-version": "MAJOR.MINOR.FIX.BUILD",
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      expect(response.status).to.eq(200);
      expect(response.body).to.have.property("points");
      expect(response.body).to.have.property("organizationId");
      expect(response.body).to.have.property("createdAt");
      expect(response.body).to.have.property("updatedAt");
    });
  }); 

  */
});
