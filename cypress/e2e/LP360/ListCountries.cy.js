describe("Get Countries", () => {
  it("List Countries", function() {
    cy.request({
      method: "GET",
      url: "https://mcs-develop.appspot.com/statics/v3/country",
    }).then((response) => {
      expect(response.status).to.eq(200);
      expect(response.body).to.have.property("countries");
      cy.wrap({ currency_code: 'USD' }).should("have.property", "currency_code")
    });
  });
});
